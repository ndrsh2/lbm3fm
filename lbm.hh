/*******************************************************************************
* Thermal Multiphase Lattice Boltzmann Solver - V1.0
* Copyright (C) 2016 Andreas Hantsch (lbm3f@gmx-topmail.de)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*******************************************************************************/


int init_density(int r, int D, double rho_vapour, double rho_liquid, vector<double>& rho, vector<double>& eps, vector<double>& ux, vector<double>& uy, vector<double>& g0, vector<double>& h0, double& intdensity0)
{
	cout << "Initializing Distribution Functions . . ";

	int x, y, pos; 
	double uxl, uyl, u2,amp0=1e-2,amp=0.5,n=1;
	srand((unsigned)time(0));

	intdensity0=0.;
	for (y=0;y<ly;y++){
		for (x=0;x<lx;x++){
			pos = posf(x,y);
// 			rho[pos] = in_circle_diffuse(x, y, 0.25*lx, 0.5*ly, r, rho_liquid, rho_vapour, double(D)); // circle in the middle
			rho[pos] = in_circle_diffuse(x, y, 0.5*lx, int(r+0*D), r, rho_liquid, rho_vapour, double(D)); // circle close to wall
// 			rho[pos]=rho_liquid; // uniform liquid
// 			rho[pos]=rho_vapour; // uniform vapour

			// film flow initialisation
// 			amp=0.5;
 //			amp=0.5*(1.-amp0*cos(n*x*2.*pi/(lx-1)));
// 			amp=0.5*(1. + amp0*rand()/double(RAND_MAX + 1.0) - 2.*amp0*amp*rand()/double(RAND_MAX + 1.0));
//			rho[pos]=0.5*(rho_liquid+rho_vapour) + 0.5*(rho_liquid-rho_vapour) * tanh(2.*(amp*(ly-1.) - y)/D); // diffuse film

			// temperature field
			eps[pos]=temperaturef(x, 0);

			// velocity field
			ux[pos]=0.;
			uy[pos]=0.;

			// initialisation with equilibrium distribution function
			// some short cuts
			uxl=ux[pos];
			uyl=uy[pos];
			u2=uxl*uxl + uyl*uyl;
			// FIXME: These equilibrium distribution functions are only valid if the velocity field is zero!
			// g0 here is in fact f0 and has to transformed correctly to g0!
			// in Lee-Fischer words: f^eq has to be transformed into fbar^eq!

			g0[9*pos + 0] = 4.0/9.0 * rho[pos] * (1.0 																													- 1.5*u2);
			g0[9*pos + 1] = 1.0/9.0 * rho[pos] * (1.0 + 3.0*( uxl     ) + 4.5*( uxl     )*( uxl     ) - 1.5*u2);
			g0[9*pos + 2] = 1.0/9.0 * rho[pos] * (1.0 + 3.0*(      uyl) + 4.5*(      uyl)*(      uyl) - 1.5*u2);
			g0[9*pos + 3] = 1.0/9.0 * rho[pos] * (1.0 + 3.0*(-uxl     ) + 4.5*(-uxl     )*(-uxl     ) - 1.5*u2);
			g0[9*pos + 4] = 1.0/9.0 * rho[pos] * (1.0 + 3.0*(    - uyl) + 4.5*(    - uyl)*(    - uyl) - 1.5*u2);
			g0[9*pos + 5] = 1.0/36. * rho[pos] * (1.0 + 3.0*( uxl + uyl) + 4.5*( uxl + uyl)*( uxl + uyl) - 1.5*u2);
			g0[9*pos + 6] = 1.0/36. * rho[pos] * (1.0 + 3.0*(-uxl + uyl) + 4.5*(-uxl + uyl)*(-uxl + uyl) - 1.5*u2);
			g0[9*pos + 7] = 1.0/36. * rho[pos] * (1.0 + 3.0*(-uxl - uyl) + 4.5*(-uxl - uyl)*(-uxl - uyl) - 1.5*u2);
			g0[9*pos + 8] = 1.0/36. * rho[pos] * (1.0 + 3.0*( uxl - uyl) + 4.5*( uxl - uyl)*( uxl - uyl) - 1.5*u2);

			// h^eq is okay, since the "normal" scheme doesn't have a hbar^eq
			h0[9*pos + 0] = 4.0/9.0 * eps[pos] * (1.0																													- 1.5*u2);
			h0[9*pos + 1] = 1.0/9.0 * eps[pos] * (1.0 + 3.0*( uxl     ) + 4.5*( uxl     )*( uxl     ) - 1.5*u2);
			h0[9*pos + 2] = 1.0/9.0 * eps[pos] * (1.0 + 3.0*(      uyl) + 4.5*(      uyl)*(      uyl) - 1.5*u2);
			h0[9*pos + 3] = 1.0/9.0 * eps[pos] * (1.0 + 3.0*(-uxl     ) + 4.5*(-uxl     )*(-uxl     ) - 1.5*u2);
			h0[9*pos + 4] = 1.0/9.0 * eps[pos] * (1.0 + 3.0*(    - uyl) + 4.5*(    - uyl)*(    - uyl) - 1.5*u2);
			h0[9*pos + 5] = 1.0/36. * eps[pos] * (1.0 + 3.0*( uxl + uyl) + 4.5*( uxl + uyl)*( uxl + uyl) - 1.5*u2);
			h0[9*pos + 6] = 1.0/36. * eps[pos] * (1.0 + 3.0*(-uxl + uyl) + 4.5*(-uxl + uyl)*(-uxl + uyl) - 1.5*u2);
			h0[9*pos + 7] = 1.0/36. * eps[pos] * (1.0 + 3.0*(-uxl - uyl) + 4.5*(-uxl - uyl)*(-uxl - uyl) - 1.5*u2);
			h0[9*pos + 8] = 1.0/36. * eps[pos] * (1.0 + 3.0*( uxl - uyl) + 4.5*( uxl - uyl)*( uxl - uyl) - 1.5*u2);

			intdensity0+=rho[pos]; // for calculating the initial integrated density
		}
	}
	cout << "done." << endl;
	return(0);
}


int calc_density(int lx, int ly, vector<double>& rho, vector<double>& g1)
{
	int i, x, y, pos;

	for (y=0;y<ly;y++){
		for (x=0;x<lx;x++){
			pos = posf(x,y);

			rho[pos] = 0.0;
			for (i=0;i<9;i++){
				rho[pos] += g1[9*pos + i];
			}
		}
	}
	return(0);
}


int calc_parameters(double& beta, double& kappa, int& D, double& sigma, double& rho_vapour, double& rho_liquid)
{
	double dr=rho_liquid - rho_vapour;
	beta = 12.*sigma/(double(D)*dr*dr*dr*dr);
	kappa = 1.5*double(D)*sigma/(dr*dr);

	return(0);
}


int calc_chem_potential(int lx, int ly, vector<double>& rho, vector<double>& eps, double& beta, double& sigma, double& gamma, double& rho_vapour, double& rho_liquid, vector<double>& mu, int D, int time, int time_init, double theta, double mufact)
{
	int x, y, pos, xe, xw, yn, ys;
	double mu0,sigmat, kappa, phi, C, laplacian, muart, gradr;
	double dr3 = (rho_liquid - rho_vapour)*(rho_liquid - rho_vapour)*(rho_liquid - rho_vapour);
	double a=acos(sin(theta)*sin(theta));
	double wetting_potential=2.*sgn(0.5*pi-theta)*sqrt(cos(a/3.)*(1.-cos(a/3.)));

	for (y=0;y<ly;y++){
		// boundary conditions
		if (y == 0)		 {ys = 1;		} else {ys = y-1;} // extrapolation bc
		if (y == ly-1) {yn = ly-2;} else {yn = y+1;} // extrapolation bc

		for (x=0;x<lx;x++){
			pos = posf(x,y);
			// boundary conditions
			if (x == 0)		 {xw = 1;		} else {xw = x-1;} // extrapolation bc
			if (x == lx-1) {xe = lx-2;} else {xe = x+1;} // extrapolation bc

			sigmat=(sigma-gamma)+gamma*(eps[pos]); // = sigmamax + gamma*(t-t_min)

			calc_parameters(beta, kappa, D, sigmat, rho_vapour, rho_liquid);
			phi=0.25*wetting_potential*(rho_liquid-rho_vapour)*(rho_liquid-rho_vapour)*sqrt(2.*kappa*beta);
			C = (rho[pos] - rho_vapour)/(rho_liquid - rho_vapour);
			gradr = phi/kappa; //linear
			mu0 = 4.*beta*(rho[pos] - rho_liquid)*(rho[pos] - rho_vapour)*(rho[pos] - 0.5*(rho_liquid + rho_vapour));
			if (y==0){ // bottom wall
				laplacian = 0.25*kappa*(4.*(rho[posf(xw,y)] + rho[posf(xe,y)] + rho[posf(x,1)]) - 13.*rho[pos] + rho[posf(x,2)] + 6.*gradr);
			}
			else if (y==ly-1){ // top wall
				laplacian = 0.25*kappa*(4.*(rho[posf(xw,y)] + rho[posf(xe,y)] + rho[posf(x,ly-2)]) - 13.*rho[pos] + rho[posf(x,ly-3)]);
			}
			else{ // bulk region
				laplacian = kappa*( 2.0/3.0*(rho[posf(xe,y)] + rho[posf(xw,y)] - 4.0*rho[pos] + rho[posf(x,yn)] + rho[posf(x,ys)]) + 1.0/6.0*(rho[posf(xe,yn)] + rho[posf(xw,yn)] - 4.0*rho[pos] + rho[posf(xe,ys)] + rho[posf(xw,ys)]) );
			}
			muart = (C<0) ? 2.*mufact*beta*C*dr3 : 0;
			mu[pos] = mu0 - laplacian + muart;
		}
	}
	return(0);
}


int calc_temperature(int lx, int ly, vector<double>& eps, vector<double>& h1)
{
	int i, x, y, pos;

	for (y=0;y<ly;y++){
		for (x=0;x<lx;x++){
			pos = posf(x,y);
 			eps[pos] = 0.0;
 			for (i=0;i<9;i++){
 				eps[pos] += h1[9*pos + i];
 			}
			eps[pos]=temperaturef(x, 0); // FIXME: predefined temperature
		}
	}
	return(0);
}



bool check_density(int lx, int ly, vector<double>& density, vector<double>& ux, vector<double>&uy, int t, double intdensity0, int dt_density) {
	double intdensity=0.;
	int x,y,pos;
	double ma=0.;
	for (y=0; y<ly; ++y) {
		for (x=0; x<lx; ++x) {
			pos=x+lx*y;
			intdensity+=density[pos];
			ma=max(ma,sqrt(3.*(ux[pos]*ux[pos] + uy[pos]*uy[pos])));
		}
	}
	if (t%(dt_density*20)==0) {
		printf("# time step\t mass dev\tmax(Ma)\n");
	}
	printf("%11d\t% .5f %%\t%.15f", t, (intdensity-intdensity0)/intdensity0*100., ma);
	cout << endl;
	return(0);
}


int streaming_step(int lx, int ly, vector<double>& g0, vector<double>& g1, vector<double>& h0, vector<double>& h1)
{
	int pos, x, y, xe, xw, yn, ys;
	for (y=0;y<ly;y++){
		// boundary conditions
		if (y==0)    {ys=-1;}else{ys=y-1;} // avoiding periodic bc
		if (y==ly-1) {yn=-1;}else{yn=y+1;} // avoiding periodic bc

		for (x=0;x<lx;x++){
			pos = posf(x,y);
			// boundary conditions
		if (x == 0)		 {xw = lx-1;} else {xw = x-1;} // periodic bc
		if (x == lx-1) {xe = 0;		} else {xe = x+1;} // periodic bc
			// streaming step
									g1[9*pos            ] = g0[9*pos    ];
									g1[9*posf(xe,y ) + 1] = g0[9*pos + 1];
			if(yn!=-1){	g1[9*posf(x ,yn) + 2] = g0[9*pos + 2];}
									g1[9*posf(xw,y ) + 3] = g0[9*pos + 3];
			if(ys!=-1){	g1[9*posf(x ,ys) + 4] = g0[9*pos + 4];}
			if(yn!=-1){	g1[9*posf(xe,yn) + 5] = g0[9*pos + 5];}
			if(yn!=-1){	g1[9*posf(xw,yn) + 6] = g0[9*pos + 6];}
			if(ys!=-1){	g1[9*posf(xw,ys) + 7] = g0[9*pos + 7];}
			if(ys!=-1){	g1[9*posf(xe,ys) + 8] = g0[9*pos + 8];}

									h1[9*pos            ] = h0[9*pos    ];
									h1[9*posf(xe,y ) + 1] = h0[9*pos + 1];
			if(yn!=-1){	h1[9*posf(x ,yn) + 2] = h0[9*pos + 2];}
									h1[9*posf(xw,y ) + 3] = h0[9*pos + 3];
			if(ys!=-1){	h1[9*posf(x ,ys) + 4] = h0[9*pos + 4];}
			if(yn!=-1){	h1[9*posf(xe,yn) + 5] = h0[9*pos + 5];}
			if(yn!=-1){	h1[9*posf(xw,yn) + 6] = h0[9*pos + 6];}
			if(ys!=-1){	h1[9*posf(xw,ys) + 7] = h0[9*pos + 7];}
			if(ys!=-1){	h1[9*posf(xe,ys) + 8] = h0[9*pos + 8];}
		}
	}
	return(0);
}


int boundary(vector<double>& g1, vector<double>& h1, vector<double>& ux, vector<double>& uy, vector<double>& rho, vector<double>& mu, vector<double>& eps, vector<double> gravity, int time, int time_init, double beta, double rho_liquid, double rho_vapour)
{
	int x,y,pos, xe, xw, yn, ys;
	double uwx=0., uwy=0., u2, fx, fy, pressure;
	double Cx_rho, Cx_mu, Cy_rho, Cy_mu, gm, gp, eps_loc;

	double epsn, epss;

	y=0; // south wall bc with moment method
	// boundary conditions
	yn=1;ys=1;
	for (x=0;x<lx;x++){
		// boundary conditions
		if (x == 0)		 {xw = lx-1;} else {xw = x-1;} // periodic bc
		if (x == lx-1) {xe = 0;		} else {xe = x+1;} // periodic bc
		pos=posf(x,y);

		ux[pos]=uwx;
		uy[pos]=uwy;
		u2=uwx*uwx + uwy*uwy;

		// calculation of force terms from old time step properties
		Cx_rho=1./3.*(rho[posf(xe,y)]-rho[posf(xw,y)]);
		Cy_rho=0.;
		Cx_mu =1./3.*( mu[posf(xe,y)]- mu[posf(xw,y)]);
		Cy_mu =0.;

		fx=1./3.*Cx_rho - rho[pos]*Cx_mu + rho[pos]*gravity[0];
		fy=1./3.*Cy_rho - rho[pos]*Cy_mu + rho[pos]*gravity[1];

		// calculation of the density at the wall and the missing incoming particle distribution functions
		rho[pos]=(g1[9*pos]+g1[9*pos+1]+g1[9*pos+3]+2.*(g1[9*pos+4]+g1[9*pos+7]+g1[9*pos+8])-0.5*fy)/(1.-uwy);
		pressure = rho[pos]*mu[pos] - beta*(rho[pos]-rho_liquid)*(rho[pos]-rho_liquid)*(rho[pos]-rho_vapour)*(rho[pos]-rho_vapour);
		g1[9*pos+2] = g1[9*pos+1] + g1[9*pos+3] + g1[9*pos+4] + 2.* (g1[9*pos+7] + g1[9*pos+8]) - pressure - rho[pos]*uwx*uwx - 0.5*fy;
		gm=rho[pos]*uwx - g1[9*pos+1] - g1[9*pos+8] + g1[9*pos+3] + g1[9*pos+7] - 0.5*fx;
		gp=pressure + rho[pos]*uwx*uwx - g1[9*pos+1] - g1[9*pos+3] - g1[9*pos+7] - g1[9*pos+8];
		g1[9*pos+5]=0.5*(gm+gp);
		g1[9*pos+6]=0.5*(gp-gm);

		// thermal bounce back south wall
		epss=temperaturef(x, time);
		eps_loc = h1[9*pos] + h1[9*pos + 1] + h1[9*pos + 3] + h1[9*pos + 4] + h1[9*pos + 7] + h1[9*pos + 8];
		h1[9*pos + 2] = 2./3.*( epss - eps_loc );
		h1[9*pos + 5] = 1./6.*( epss - eps_loc );
		h1[9*pos + 6] = 1./6.*( epss - eps_loc );
	}

	y=ly-1; // on node bounce back north wall
	ys=ly-2;yn=ly-2;
	for (x=0;x<lx;x++){
		// boundary conditions
		if (x == 0)		 {xw = lx-1;} else {xw = x-1;} // periodic bc
		if (x == lx-1) {xe = 0;		} else {xe = x+1;} // periodic bc
		pos=posf(x,y);

		ux[pos]=uwx;
		uy[pos]=uwy;
		u2=uwx*uwx + uwy*uwy;

		// calculation of force terms from old time step properties
		Cx_rho=1./3.*(rho[posf(xe,y)]-rho[posf(xw,y)]) + 1./12.*(rho[posf(xe,yn)]-rho[posf(xw,ys)]+rho[posf(xe,ys)]-rho[posf(xw,yn)]);
		Cy_rho=1./3.*(rho[posf(x,yn)]-rho[posf(x,ys)]) + 1./12.*(rho[posf(xe,yn)]-rho[posf(xw,ys)]+rho[posf(xw,yn)]-rho[posf(xe,ys)]);
		Cx_mu =1./3.*( mu[posf(xe,y)]- mu[posf(xw,y)]) + 1./12.*( mu[posf(xe,yn)]- mu[posf(xw,ys)]+ mu[posf(xe,ys)]- mu[posf(xw,yn)]);
		Cy_mu =1./3.*( mu[posf(x,yn)]- mu[posf(x,ys)]) + 1./12.*( mu[posf(xe,yn)]- mu[posf(xw,ys)]+ mu[posf(xw,yn)]- mu[posf(xe,ys)]);

		fx=1./3.*Cx_rho - rho[pos]*Cx_mu + rho[pos]*gravity[0];
		fy=1./3.*Cy_rho - rho[pos]*Cy_mu + rho[pos]*gravity[1];

		// calculation of the density at the wall and the missing incoming particle distribution functions
		rho[pos]=(g1[9*pos]+g1[9*pos+1]+g1[9*pos+3]+2.*(g1[9*pos+2]+g1[9*pos+5]+g1[9*pos+6])-0.5*fy)/(1.-uwy);
		pressure = rho[pos]*mu[pos] - beta*(rho[pos]-rho_liquid)*(rho[pos]-rho_liquid)*(rho[pos]-rho_vapour)*(rho[pos]-rho_vapour);
		g1[9*pos+4] = g1[9*pos+1] + g1[9*pos+2] + g1[9*pos+3] + 2.*( g1[9*pos+5] + g1[9*pos+6] ) + 0.5*fy - pressure - rho[pos]*uwx*uwx;
		gm=-rho[pos]*uwx - g1[9*pos+3] - g1[9*pos+6] + g1[9*pos+1] + g1[9*pos+5] + 0.5*fx;
		gp=pressure + rho[pos]*uwx*uwx - g1[9*pos+1] - g1[9*pos+3] - g1[9*pos+5] - g1[9*pos+6];
		g1[9*pos+7]=0.5*(gm+gp);
		g1[9*pos+8]=0.5*(gp-gm);

		// thermal bounce back north wall
		epsn=eps[posf(x,ys)];
		eps_loc = h1[9*pos] + h1[9*pos + 1] + h1[9*pos + 2] + h1[9*pos + 3] + h1[9*pos + 5] + h1[9*pos + 6];
		h1[9*pos + 4] = 2./3.*( epsn - eps_loc );
		h1[9*pos + 7] = 1./6.*( epsn - eps_loc );
		h1[9*pos + 8] = 1./6.*( epsn - eps_loc );
	}
	return(0);
}

int collision_step(int lx, int ly, double& nu, vector<double>& rho, vector<double>& eps, vector<double>& mu, vector<double>& ux, vector<double>& uy, vector<double>& g0, vector<double>& g1, vector<double>& h0, vector<double>& h1, vector<double> gravity, vector<double> dp, double rho_liquid, double rho_vapour, double tau_liquid, double tau_vapour, int D)
{
	int i, x, y, pos, xe, xw, yn, ys, xee, xww, ynn, yss;
	double uxl, uyl, feq[9], geq[9], heq[9], force[9], u2, tau = 3.0*nu;
	double Cx_rho, Cy_rho, Cx_mu, Cy_mu, Bx_rho, By_rho, Bx_mu, By_mu, Mx_rho, My_rho, Mx_mu, My_mu;
	double sum=0.;
	double tauh=.01;
	double tauh_liquid=0.5, tauh_vapour=.25;

	for (y=0;y<ly;y++){
		// boundary conditions
		if (y == 0)			{ys  = 1;			yss = 2;		} else {ys = y-1; yss = y-2;}	// extrapolation bc
		if (y == 1)			{yss = 1;									}															// extrapolation bc
		if (y == ly-1)	{yn  = ly-2;	ynn = ly-3;	} else {yn = y+1; ynn = y+2;}	// extrapolation bc
		if (y == ly-2)	{ynn = ly-2;							}															// extrapolation bc

		for (x=0;x<lx;x++){
			pos = posf(x,y);
			// boundary conditions
			if (x == 0)			{xw  = lx-1;	xww = lx-2;	} else {xw = x-1; xww = x-2;}	// periodic bc
			if (x == 1)			{xww = lx-1;							}															// periodic bc
			if (x == lx-1)	{xe  = 0;			xee = 1;		} else {xe = x+1; xee = x+2;}	// periodic bc
			if (x == lx-2)	{xee = 0;									}															// periodic bc

			// different gradient schemes for non-directive derivatives
			// Cx - Central Differencing Scheme in x-Direction
			// Bx - Biased Differencing Scheme in x-Direction
			// Mx - Mixed Differencing Scheme in x-Direction
			if (y==0){ // at bottom wall
				Cx_rho=1./3.*(rho[posf(xe,y)]-rho[posf(xw,y)]);
				Cy_rho=0.;
				Cx_mu =1./3.*( mu[posf(xe,y)]- mu[posf(xw,y)]);
				Cy_mu =0.;

				Bx_rho=1./6.*(rho[posf(xww,y)]-rho[posf(xee,y)] + 4.*(rho[posf(xe,y)]-rho[posf(xw,y)]));
				By_rho=0.;
				Bx_mu =1./6.*( mu[posf(xww,y)]- mu[posf(xee,y)] + 4.*( mu[posf(xe,y)]- mu[posf(xw,y)]));
				By_mu =0.;
			}
			else{ // at all other nodes
				Cx_rho=1./3.*(rho[posf(xe,y)]-rho[posf(xw,y)]) + 1./12.*(rho[posf(xe,yn)]-rho[posf(xw,ys)]+rho[posf(xe,ys)]-rho[posf(xw,yn)]);
				Cy_rho=1./3.*(rho[posf(x,yn)]-rho[posf(x,ys)]) + 1./12.*(rho[posf(xe,yn)]-rho[posf(xw,ys)]+rho[posf(xw,yn)]-rho[posf(xe,ys)]);
				Cx_mu =1./3.*( mu[posf(xe,y)]- mu[posf(xw,y)]) + 1./12.*( mu[posf(xe,yn)]- mu[posf(xw,ys)]+ mu[posf(xe,ys)]- mu[posf(xw,yn)]);
				Cy_mu =1./3.*( mu[posf(x,yn)]- mu[posf(x,ys)]) + 1./12.*( mu[posf(xe,yn)]- mu[posf(xw,ys)]+ mu[posf(xw,yn)]- mu[posf(xe,ys)]);

				if(y==1){ // first fluid node layer above the wall
					Bx_rho=1./6.*(rho[posf(xww,y)]-rho[posf(xee,y)] + 4.*(rho[posf(xe,y)]-rho[posf(xw,y)])) + 1./24.*(-rho[posf(xee,ynn)]+rho[posf(xww,ynn)] + 4.*(rho[posf(xe,yn)]-rho[posf(xw,ys)]+rho[posf(xe,ys)]-rho[posf(xw,yn)]));
					By_rho=1./6.*(rho[pos]-rho[posf(x,ynn)] + 4.*(rho[posf(x,yn)]-rho[posf(x,ys)])) + 1./24.*(2.*rho[pos]-rho[posf(xee,ynn)]-rho[posf(xww,ynn)] + 4.*(rho[posf(xe,yn)]-rho[posf(xw,ys)]-rho[posf(xe,ys)]+rho[posf(xw,yn)]));
					Bx_mu =1./6.*( mu[posf(xww,y)]- mu[posf(xee,y)] + 4.*( mu[posf(xe,y)]- mu[posf(xw,y)])) + 1./24.*( - mu[posf(xee,ynn)]+ mu[posf(xww,ynn)] + 4.*( mu[posf(xe,yn)]- mu[posf(xw,ys)]+ mu[posf(xe,ys)]- mu[posf(xw,yn)]));
					By_mu =1./6.*( mu[pos]- mu[posf(x,ynn)] + 4.*( mu[posf(x,yn)]- mu[posf(x,ys)])) + 1./24.*( 2.*mu[pos]- mu[posf(xee,ynn)]- mu[posf(xww,ynn)] + 4.*( mu[posf(xe,yn)]- mu[posf(xw,ys)]- mu[posf(xe,ys)]+ mu[posf(xw,yn)]));
				}
				else{
					Bx_rho=1./6.*(rho[posf(xww,y)]-rho[posf(xee,y)] + 4.*(rho[posf(xe,y)]-rho[posf(xw,y)])) + 1./24.*(rho[posf(xww,yss)]-rho[posf(xee,ynn)]+rho[posf(xww,ynn)]-rho[posf(xee,yss)] + 4.*(rho[posf(xe,yn)]-rho[posf(xw,ys)]+rho[posf(xe,ys)]-rho[posf(xw,yn)]));
					By_rho=1./6.*(rho[posf(x,yss)]-rho[posf(x,ynn)] + 4.*(rho[posf(x,yn)]-rho[posf(x,ys)])) + 1./24.*(rho[posf(xww,yss)]-rho[posf(xee,ynn)]+rho[posf(xee,yss)]-rho[posf(xww,ynn)] + 4.*(rho[posf(xe,yn)]-rho[posf(xw,ys)]-rho[posf(xe,ys)]+rho[posf(xw,yn)]));
					Bx_mu =1./6.*( mu[posf(xww,y)]- mu[posf(xee,y)] + 4.*( mu[posf(xe,y)]- mu[posf(xw,y)])) + 1./24.*( mu[posf(xww,yss)]- mu[posf(xee,ynn)]+ mu[posf(xww,ynn)]- mu[posf(xee,yss)] + 4.*( mu[posf(xe,yn)]- mu[posf(xw,ys)]+ mu[posf(xe,ys)]- mu[posf(xw,yn)]));
					By_mu =1./6.*( mu[posf(x,yss)]- mu[posf(x,ynn)] + 4.*( mu[posf(x,yn)]- mu[posf(x,ys)])) + 1./24.*( mu[posf(xww,yss)]- mu[posf(xee,ynn)]+ mu[posf(xee,yss)]- mu[posf(xww,ynn)] + 4.*( mu[posf(xe,yn)]- mu[posf(xw,ys)]- mu[posf(xe,ys)]+ mu[posf(xw,yn)]));
				}
			}

			Mx_rho = 0.5*( Cx_rho + Bx_rho );
			My_rho = 0.5*( Cy_rho + By_rho );
			Mx_mu  = 0.5*(  Cx_mu +  Bx_mu );
			My_mu  = 0.5*(  Cy_mu +  By_mu );

			// velocity components
			ux[pos] = ( g1[9*pos+1] + g1[9*pos+5] + g1[9*pos+8] - ( g1[9*pos+3] + g1[9*pos+6] + g1[9*pos+7] ) + 0.5*(1.0/3.0 * Cx_rho - rho[pos] * Cx_mu + rho[pos]*gravity[0] + dp[0])) / rho[pos];
			uy[pos] = ( g1[9*pos+2] + g1[9*pos+5] + g1[9*pos+6] - ( g1[9*pos+4] + g1[9*pos+7] + g1[9*pos+8] ) + 0.5*(1.0/3.0 * Cy_rho - rho[pos] * Cy_mu + rho[pos]*gravity[1] + dp[1])) / rho[pos];

			uxl=ux[pos];
			uyl=uy[pos];
			u2 = uxl*uxl + uyl*uyl;

			// second-order central differencing scheme with directional derivatives
			if (y==0){
				force[0] = - 1.0/3.0 * ( uxl*Cx_rho ) + rho[pos] * ( uxl*Cx_mu - uxl*gravity[0] - uyl*gravity[1]) + uxl*dp[0] + uyl*dp[1];
				force[1] =   1.0/6.0 * ( rho[posf(xe,y)] - rho[posf(xw,y)] ) - 0.5*rho[pos] * ( mu[posf(xe,y)] - mu[posf(xw,y)] - 2.*gravity[0]) + dp[0]	+ force[0];
				force[2] =   -rho[pos] * ( -gravity[1]) + dp[1] + force[0];
				force[3] =   1.0/6.0 * ( rho[posf(xw,y)] - rho[posf(xe,y)] ) - 0.5*rho[pos] * ( mu[posf(xw,y)] - mu[posf(xe,y)] + 2.*gravity[0]) - dp[0] + force[0];
				force[4] =   -rho[pos] * (+gravity[1]) - dp[1] + force[0];
				force[5] =   -rho[pos] * (-gravity[0] - gravity[1]) + dp[0] + dp[1] + force[0];
				force[6] =   -rho[pos] * (+gravity[0] - gravity[1]) - dp[0] + dp[1] + force[0];
				force[7] =   -rho[pos] * (+gravity[0] + gravity[1]) - dp[0] - dp[1] + force[0];
				force[8] =   -rho[pos] * (-gravity[0] + gravity[1]) + dp[1] - dp[1] + force[0];
			}
			else{
				force[0] = - 1.0/3.0 * ( uxl*Cx_rho + uyl*Cy_rho ) + rho[pos] * ( uxl*Cx_mu + uyl*Cy_mu - uxl*gravity[0] - uyl*gravity[1]) + uxl*dp[0] + uyl*dp[1];
				force[1] =   1.0/6.0 * ( rho[posf(xe,y)] - rho[posf(xw,y)] ) - 0.5*rho[pos] * ( mu[posf(xe,y)] - mu[posf(xw,y)] - 2.*gravity[0]) + dp[0] + force[0];
				force[2] =   1.0/6.0 * ( rho[posf(x,yn)] - rho[posf(x,ys)] ) - 0.5*rho[pos] * ( mu[posf(x,yn)] - mu[posf(x,ys)] - 2.*gravity[1]) + dp[1] + force[0];
				force[3] =   1.0/6.0 * ( rho[posf(xw,y)] - rho[posf(xe,y)] ) - 0.5*rho[pos] * ( mu[posf(xw,y)] - mu[posf(xe,y)] + 2.*gravity[0]) - dp[0] + force[0];
				force[4] =   1.0/6.0 * ( rho[posf(x,ys)] - rho[posf(x,yn)] ) - 0.5*rho[pos] * ( mu[posf(x,ys)] - mu[posf(x,yn)] + 2.*gravity[1]) - dp[1] + force[0];
				force[5] =   1.0/6.0 * ( rho[posf(xe,yn)] - rho[posf(xw,ys)] ) - 0.5*rho[pos] * ( mu[posf(xe,yn)] - mu[posf(xw,ys)] - 2.*gravity[0] - 2.*gravity[1]) + dp[0] + dp[1] + force[0];
				force[6] =   1.0/6.0 * ( rho[posf(xw,yn)] - rho[posf(xe,ys)] ) - 0.5*rho[pos] * ( mu[posf(xw,yn)] - mu[posf(xe,ys)] + 2.*gravity[0] - 2.*gravity[1]) - dp[0] + dp[1] + force[0];
				force[7] =   1.0/6.0 * ( rho[posf(xw,ys)] - rho[posf(xe,yn)] ) - 0.5*rho[pos] * ( mu[posf(xw,ys)] - mu[posf(xe,yn)] + 2.*gravity[0] + 2.*gravity[1]) - dp[0] - dp[1] + force[0];
				force[8] =   1.0/6.0 * ( rho[posf(xe,ys)] - rho[posf(xw,yn)] ) - 0.5*rho[pos] * ( mu[posf(xe,ys)] - mu[posf(xw,yn)] - 2.*gravity[0] + 2.*gravity[1]) + dp[0] - dp[1] + force[0];
			}

			// conventional equilibrium distribution function
			feq[0] = 4.0/9.0 * rho[pos] * (1.0 																													- 1.5*u2);
			feq[1] = 1.0/9.0 * rho[pos] * (1.0 + 3.0*( uxl     ) + 4.5*( uxl     )*( uxl     ) - 1.5*u2);
			feq[2] = 1.0/9.0 * rho[pos] * (1.0 + 3.0*(      uyl) + 4.5*(      uyl)*(      uyl) - 1.5*u2);
			feq[3] = 1.0/9.0 * rho[pos] * (1.0 + 3.0*(-uxl     ) + 4.5*(-uxl     )*(-uxl     ) - 1.5*u2);
			feq[4] = 1.0/9.0 * rho[pos] * (1.0 + 3.0*(    - uyl) + 4.5*(    - uyl)*(    - uyl) - 1.5*u2);
			feq[5] = 1.0/36. * rho[pos] * (1.0 + 3.0*( uxl + uyl) + 4.5*( uxl + uyl)*( uxl + uyl) - 1.5*u2);
			feq[6] = 1.0/36. * rho[pos] * (1.0 + 3.0*(-uxl + uyl) + 4.5*(-uxl + uyl)*(-uxl + uyl) - 1.5*u2);
			feq[7] = 1.0/36. * rho[pos] * (1.0 + 3.0*(-uxl - uyl) + 4.5*(-uxl - uyl)*(-uxl - uyl) - 1.5*u2);
			feq[8] = 1.0/36. * rho[pos] * (1.0 + 3.0*( uxl - uyl) + 4.5*( uxl - uyl)*( uxl - uyl) - 1.5*u2);

			for (i=0;i<9;i++){
				geq[i] = (1.0 - 1.5 * force[i] / rho[pos]) * feq[i];
			}

			// second-order mixed differencing scheme with directional derivatives
			if (y==0){
				force[0] = - 1.0/3.0 * ( uxl*Mx_rho + uyl*My_rho ) + rho[pos] * ( uxl*Mx_mu + uyl*My_mu - uxl*gravity[0] - uyl*gravity[1] ) + uxl*dp[0] + uyl*dp[1];
				force[1] = 1.0/12.0 * ( -rho[posf(xee,y)] + 5.*rho[posf(xe,y)] - 3.*rho[pos] - rho[posf(xw,y)] ) - 0.25*rho[pos] * ( -mu[posf(xee,y)] + 5.*mu[posf(xe,y)] - 3.*mu[pos] - mu[posf(xw,y)] - 4.*gravity[0] ) + dp[0] + force[0];
				force[2] = 1.0/12.0 * ( -rho[posf(x,ynn)] + 5.*rho[posf(x,yn)] - 3.*rho[pos] - rho[posf(x,yn)] ) - 0.25*rho[pos] * ( -mu[posf(x,ynn)] + 5.*mu[posf(x,yn)] - 3.*mu[pos] - mu[posf(x,yn)] - 4.*gravity[1] ) + dp[1] + force[0];
				force[3] = 1.0/12.0 * ( -rho[posf(xww,y)] + 5.*rho[posf(xw,y)] - 3.*rho[pos] - rho[posf(xe,y)] ) - 0.25*rho[pos] * ( -mu[posf(xww,y)] + 5.*mu[posf(xw,y)] - 3.*mu[pos] - mu[posf(xe,y)] + 4.*gravity[0] ) - dp[0] + force[0];
				force[4] = 1.0/12.0 * ( -rho[posf(x,ynn)] + 5.*rho[posf(x,yn)] - 3.*rho[pos] - rho[posf(x,yn)] ) - 0.25*rho[pos] * ( -mu[posf(x,ynn)] + 5.*mu[posf(x,yn)] - 3.*mu[pos] - mu[posf(x,yn)] + 4.*gravity[1] ) - dp[1] + force[0];
				force[5] = 1.0/12.0 * ( -rho[posf(xee,ynn)] + 5.*rho[posf(xe,yn)] - 3.*rho[pos] - rho[posf(xe,yn)] ) - 0.25*rho[pos] * ( -mu[posf(xee,ynn)] + 5.*mu[posf(xe,yn)] - 3.*mu[pos] - mu[posf(xe,yn)] - 4.*gravity[0] - 4.*gravity[1] ) + dp[0] + dp[1] + force[0];
				force[6] = 1.0/12.0 * ( -rho[posf(xww,ynn)] + 5.*rho[posf(xw,yn)] - 3.*rho[pos] - rho[posf(xw,yn)] ) - 0.25*rho[pos] * ( -mu[posf(xww,ynn)] + 5.*mu[posf(xw,yn)] - 3.*mu[pos] - mu[posf(xw,yn)] + 4.*gravity[0] - 4.*gravity[1] ) - dp[0] + dp[1] + force[0];
				force[7] = 1.0/12.0 * ( -rho[posf(xee,ynn)] + 5.*rho[posf(xe,yn)] - 3.*rho[pos] - rho[posf(xe,yn)] ) - 0.25*rho[pos] * ( -mu[posf(xee,ynn)] + 5.*mu[posf(xe,yn)] - 3.*mu[pos] - mu[posf(xe,yn)] + 4.*gravity[0] + 4.*gravity[1] ) - dp[0] - dp[0] + force[0];
				force[8] = 1.0/12.0 * ( -rho[posf(xww,ynn)] + 5.*rho[posf(xw,yn)] - 3.*rho[pos] - rho[posf(xw,yn)] ) - 0.25*rho[pos] * ( -mu[posf(xww,ynn)] + 5.*mu[posf(xw,yn)] - 3.*mu[pos] - mu[posf(xw,yn)] - 4.*gravity[0] + 4.*gravity[1] ) + dp[1] - dp[1] + force[0];
			}
			else if (y==1){
				force[0] = - 1.0/3.0 * ( uxl*Mx_rho + uyl*My_rho ) + rho[pos] * ( uxl*Mx_mu + uyl*My_mu - uxl*gravity[0] - uyl*gravity[1] ) + uxl*dp[0] + uyl*dp[1];
				force[1] = 1.0/12.0 * ( -rho[posf(xee,y)] + 5.*rho[posf(xe,y)] - 3.*rho[pos] - rho[posf(xw,y)] ) - 0.25*rho[pos] * ( -mu[posf(xee,y)] + 5.*mu[posf(xe,y)] - 3.*mu[pos] - mu[posf(xw,y)] + 4.*gravity[0] ) + dp[0] + force[0];
				force[2] = 1.0/12.0 * ( -rho[posf(x,ynn)] + 5.*rho[posf(x,yn)] - 3.*rho[pos] - rho[posf(x,ys)] ) - 0.25*rho[pos] * ( -mu[posf(x,ynn)] + 5.*mu[posf(x,yn)] - 3.*mu[pos] - mu[posf(x,ys)] + 4.*gravity[1] ) + dp[1] + force[0];
				force[3] = 1.0/12.0 * ( -rho[posf(xww,y)] + 5.*rho[posf(xw,y)] - 3.*rho[pos] - rho[posf(xe,y)] ) - 0.25*rho[pos] * ( -mu[posf(xww,y)] + 5.*mu[posf(xw,y)] - 3.*mu[pos] - mu[posf(xe,y)] - 4.*gravity[0] ) - dp[0] + force[0];
				force[4] = 1.0/12.0 * ( -rho[posf(x,y  )] + 5.*rho[posf(x,ys)] - 3.*rho[pos] - rho[posf(x,yn)] ) - 0.25*rho[pos] * ( -mu[posf(x,y  )] + 5.*mu[posf(x,ys)] - 3.*mu[pos] - mu[posf(x,yn)] - 4.*gravity[1] ) - dp[1] + force[0];
				force[5] = 1.0/12.0 * ( -rho[posf(xee,ynn)] + 5.*rho[posf(xe,yn)] - 3.*rho[pos] - rho[posf(xw,ys)] ) - 0.25*rho[pos] * ( -mu[posf(xee,ynn)] + 5.*mu[posf(xe,yn)] - 3.*mu[pos] - mu[posf(xw,ys)] - 4.*gravity[0] - 4.*gravity[1] ) + dp[0] + dp[1] + force[0];
				force[6] = 1.0/12.0 * ( -rho[posf(xww,ynn)] + 5.*rho[posf(xw,yn)] - 3.*rho[pos] - rho[posf(xe,ys)] ) - 0.25*rho[pos] * ( -mu[posf(xww,ynn)] + 5.*mu[posf(xw,yn)] - 3.*mu[pos] - mu[posf(xe,ys)] + 4.*gravity[0] - 4.*gravity[1] ) - dp[0] + dp[1] + force[0];
				force[7] = 1.0/12.0 * ( -rho[posf(x  ,y  )] + 5.*rho[posf(xw,ys)] - 3.*rho[pos] - rho[posf(xe,yn)] ) - 0.25*rho[pos] * ( -mu[posf(x  ,y  )] + 5.*mu[posf(xw,ys)] - 3.*mu[pos] - mu[posf(xe,yn)] + 4.*gravity[0] + 4.*gravity[1] ) - dp[0] - dp[1] + force[0];
				force[8] = 1.0/12.0 * ( -rho[posf(x  ,y  )] + 5.*rho[posf(xe,ys)] - 3.*rho[pos] - rho[posf(xw,yn)] ) - 0.25*rho[pos] * ( -mu[posf(x  ,y  )] + 5.*mu[posf(xe,ys)] - 3.*mu[pos] - mu[posf(xw,yn)] - 4.*gravity[0] + 4.*gravity[1] ) + dp[0] - dp[1] + force[0];
			}
			else{
				force[0] = - 1.0/3.0 * ( uxl*Mx_rho + uyl*My_rho ) + rho[pos] * ( uxl*Mx_mu + uyl*My_mu - uxl*gravity[0] - uyl*gravity[1] ) + uxl*dp[0] + uyl*dp[1];

				force[1] = 1.0/12.0 * ( -rho[posf(xee,y)] + 5.*rho[posf(xe,y)] - 3.*rho[pos] - rho[posf(xw,y)] ) - 0.25*rho[pos] * ( -mu[posf(xee,y)] + 5.*mu[posf(xe,y)] - 3.*mu[pos] - mu[posf(xw,y)] - 4.*gravity[0] ) + dp[0] + force[0];
				force[2] = 1.0/12.0 * ( -rho[posf(x,ynn)] + 5.*rho[posf(x,yn)] - 3.*rho[pos] - rho[posf(x,ys)] ) - 0.25*rho[pos] * ( -mu[posf(x,ynn)] + 5.*mu[posf(x,yn)] - 3.*mu[pos] - mu[posf(x,ys)] - 4.*gravity[1] ) + dp[1] + force[0];
				force[3] = 1.0/12.0 * ( -rho[posf(xww,y)] + 5.*rho[posf(xw,y)] - 3.*rho[pos] - rho[posf(xe,y)] ) - 0.25*rho[pos] * ( -mu[posf(xww,y)] + 5.*mu[posf(xw,y)] - 3.*mu[pos] - mu[posf(xe,y)] + 4.*gravity[0] ) - dp[0] + force[0];
				force[4] = 1.0/12.0 * ( -rho[posf(x,yss)] + 5.*rho[posf(x,ys)] - 3.*rho[pos] - rho[posf(x,yn)] ) - 0.25*rho[pos] * ( -mu[posf(x,yss)] + 5.*mu[posf(x,ys)] - 3.*mu[pos] - mu[posf(x,yn)] + 4.*gravity[1] ) - dp[1] + force[0];

				force[5] = 1.0/12.0 * ( -rho[posf(xee,ynn)] + 5.*rho[posf(xe,yn)] - 3.*rho[pos] - rho[posf(xw,ys)] ) - 0.25*rho[pos] * ( -mu[posf(xee,ynn)] + 5.*mu[posf(xe,yn)] - 3.*mu[pos] - mu[posf(xw,ys)] - 4.*gravity[0] - 4.*gravity[1] ) + dp[0] + dp[1] + force[0];
				force[6] = 1.0/12.0 * ( -rho[posf(xww,ynn)] + 5.*rho[posf(xw,yn)] - 3.*rho[pos] - rho[posf(xe,ys)] ) - 0.25*rho[pos] * ( -mu[posf(xww,ynn)] + 5.*mu[posf(xw,yn)] - 3.*mu[pos] - mu[posf(xe,ys)] + 4.*gravity[0] - 4.*gravity[1] ) - dp[0] + dp[1] + force[0];
				force[7] = 1.0/12.0 * ( -rho[posf(xww,yss)] + 5.*rho[posf(xw,ys)] - 3.*rho[pos] - rho[posf(xe,yn)] ) - 0.25*rho[pos] * ( -mu[posf(xww,yss)] + 5.*mu[posf(xw,ys)] - 3.*mu[pos] - mu[posf(xe,yn)] + 4.*gravity[0] + 4.*gravity[1] ) - dp[0] - dp[1] + force[0];
				force[8] = 1.0/12.0 * ( -rho[posf(xee,yss)] + 5.*rho[posf(xe,ys)] - 3.*rho[pos] - rho[posf(xw,yn)] ) - 0.25*rho[pos] * ( -mu[posf(xee,yss)] + 5.*mu[posf(xe,ys)] - 3.*mu[pos] - mu[posf(xw,yn)] - 4.*gravity[0] + 4.*gravity[1] ) + dp[0] - dp[1] + force[0];
			}

			// relaxation time
			if (nu<0.000001){
				tau=(tau_liquid-tau_vapour)/(rho_liquid-rho_vapour)*(rho[pos]-rho_vapour)+tau_vapour;
			}

// 			tauh=(tauh_liquid-tauh_vapour)/(rho_liquid-rho_vapour)*(rho[pos]-rho_vapour)+tauh_vapour; // smooth transition from liquid to gas thermal diffusivity
// 			if (y<0.5*ly){tauh=tauh_liquid;}else{tauh=tauh_vapour;} // non-steady transition from liquid to gas thermal diffusivity
// 				if (y<0.5*ly){tau=tau_liquid;}else{tau=tau_vapour;} // non-steady transition from liquid to gas diffusivity
// 			tau=0.5*(tau_liquid+tau_vapour) + 0.5*(tau_liquid-tau_vapour) * tanh(2.*(0.5*(ly+1.) - y)/D);

			// conventional equilibrium distribution function for heat transfer distribution
			heq[0] = 4.0/9.0 * eps[pos] * (1.0																													- 1.5*u2);
			heq[1] = 1.0/9.0 * eps[pos] * (1.0 + 3.0*( uxl     ) + 4.5*( uxl     )*( uxl     ) - 1.5*u2);
			heq[2] = 1.0/9.0 * eps[pos] * (1.0 + 3.0*(      uyl) + 4.5*(      uyl)*(      uyl) - 1.5*u2);
			heq[3] = 1.0/9.0 * eps[pos] * (1.0 + 3.0*(-uxl     ) + 4.5*(-uxl     )*(-uxl     ) - 1.5*u2);
			heq[4] = 1.0/9.0 * eps[pos] * (1.0 + 3.0*(    - uyl) + 4.5*(    - uyl)*(    - uyl) - 1.5*u2);
			heq[5] = 1.0/36. * eps[pos] * (1.0 + 3.0*( uxl + uyl) + 4.5*( uxl + uyl)*( uxl + uyl) - 1.5*u2);
			heq[6] = 1.0/36. * eps[pos] * (1.0 + 3.0*(-uxl + uyl) + 4.5*(-uxl + uyl)*(-uxl + uyl) - 1.5*u2);
			heq[7] = 1.0/36. * eps[pos] * (1.0 + 3.0*(-uxl - uyl) + 4.5*(-uxl - uyl)*(-uxl - uyl) - 1.5*u2);
			heq[8] = 1.0/36. * eps[pos] * (1.0 + 3.0*( uxl - uyl) + 4.5*( uxl - uyl)*( uxl - uyl) - 1.5*u2);

			// collision step
			sum=0.;
			for (i=0;i<9;i++){
				g0[9*pos+i] = g1[9*pos+i] - 1.0 / (tau + 0.5) * (g1[9*pos+i] - geq[i]) + 3.0 * force[i] * feq[i] / rho[pos];
				sum+=g0[9*pos+i];

				h0[9*pos+i] = h1[9*pos+i] - 1.0 / (tauh + 0.5) * (h1[9*pos+i] - heq[i]);
			}
			if (sum<0){cout << "\n\nFatal Error: Negative density! Exit.\n"; exit(1);}
		}
	}
	return(0);
}
