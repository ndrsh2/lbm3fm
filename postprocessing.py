#!/usr/bin/env python
# -*- coding: utf-8 -*-
################################################################################
# Thermal Multiphase Lattice Boltzmann Solver - V1.0
# Copyright (C) 2016 Andreas Hantsch (lbm3f@gmx-topmail.de)
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
################################################################################

import sys, math, glob, os, time

timefactor=1.
velocityfactor=5.

print "Running postprocessing..."
print "Reading ./system/controlDict...",
controlDict = open("./system/controlDict")
for line in controlDict:
	if line.find("lx")>-1:
		item = line.split()
		lx=float(item[1])
	if line.find("ly")>-1:
		item = line.split()
		ly=float(item[1])
print "\bdone."
controlDict.close()
ratio=float(ly)/float(lx)

os.chdir("./results/")
print "Removing old image files...",
for file in glob.glob('*.png')+glob.glob('*.eps')+glob.glob('*.jpg'):
	os.remove(file)
print "\bdone."

print "Running gnuplot..."
number_files=len(glob.glob('*_m.ssv'))
count=0.
dc=0.0
for file in glob.glob('*_m.ssv'):
	file_number=file.replace("_m.ssv","")
	if ((float(count)/float(number_files))>=dc):
		print ("%3.0f%%" %(float(count)/float(number_files)*100))
		dc=dc+0.1
	tempfile=open("temp.plt", 'w')
	tempfile.write("""# temporary plot file
#set terminal postscript enhanced 11 size 14cm,10.5cm
set terminal png enhanced 12 size 1024,768
set out \"%s.png\"
set multiplot title \"time step: %.f\"
set size ratio %.15f

set tmargin at screen .92
set bmargin at screen 0.30
set xrange[0:1]
set yrange[0:1]
set cbrange[.1:1]
unset colorbox
unset xtics
set ytics 0.1
#set contour base
#set pm3d
set palette grey negative
unset key
set view map
set grid lt 1 lc rgb "grey"
set format x \"%%.1f\"
set format y \"%%.1f\"
set ylabel \"dimensionless position y/L_y\" offset -0.75,1
set cntrparam levels discrete .2,.55,.9
#set cntrparam levels discrete .1,.2,.3,.4,.5,.6,.7,.8,.9
splot \"%s_m.ssv\" using ($1/(%d-1)):($2/(%d-1)):3 ti \"\" w pm3d,\
\"%s_m.ssv\" using ($1/(%d-1)):($2/(%d-1)):($1*0.):(%.15f*($4)):(%.15f*$5):($1*0.) every 20:5 ti \"\" with vectors head filled lt 2  lc rgb \"blue\"

set lmargin at screen 0.165
set rmargin at screen 0.835
set bmargin at screen 0.13
set tmargin at screen 0.26

set grid
unset colorbox
set xlabel \"dimensionless position x/L_x\"
set ylabel \"dim.less temperature\" offset -1.85,0
set format y \"%%.f\"
#set ylabel \"dim.less temp.\" #at 10,0.15
set yrange [-0.1:1.1]
set xtics
set ytics 1
plot \"%s_w.ssv\" using ($1/(%d-1)):2 w l lt 1 lw 2  lc rgb \"red\"

"""
%(file_number, (float(file_number)*timefactor), ratio, file_number, lx, ly, file_number, lx, ly, velocityfactor, velocityfactor, file_number, lx))
	tempfile.close()
	os.system("gnuplot temp.plt &")
	time.sleep(1)
	os.remove("temp.plt")
	#os.system("convert -rotate 90 -geometry 1024x1024 -density 300x300 -flatten %s.eps %s.png" %(file_number, file_number))
	count=count+1.

print ("100%")
print "\bdone."


#print "Video generation..."
#os.system("mkvideo.py -f multi.avi")
print "done."
print "done."
