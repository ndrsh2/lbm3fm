/*******************************************************************************
* Thermal Multiphase Lattice Boltzmann Solver - V1.0
* Copyright (C) 2016 Andreas Hantsch (lbm3f@gmx-topmail.de)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*******************************************************************************/


const double pi=3.141592653589793238462643383279502884197;

double temperaturef(int x,int time)
{
	return(1);
// 	if (x<0.5*lx){return(0.);}else{return(1.);}
// 	return(x/(lx-1.)); // linear dependency
// 	return(double(x)/double(ly-1));
// 		return(1.-(0.5 + 0.5 * cos((x)/(ly/20.*pi))));
// 	if (x<0.5*lx){return(1.);}else{return(0.);}
// 	return (t=0.5 + 0.5 * tanh(2.*(0.5*lx - x)/4.)); // left half is warm, right one cold
// 		return(0.5 + 0.5 * cos(x/(lx/20.*pi)));// left warm, middle cold, right warm
// 		return(1.-(0.5 + 0.5 * cos((x)/(lx/20.*pi))));// left cold, middle warm, right cold
// 		if ((x<0.5*(lx-1)) || (x>0.9*(lx-1))){return(0.);}else{return(1.);}
// 	if (x<(0.5+0.5*cos(double(time-100000.)/50000.))*lx){return(1.);}else{return(0.);}
}

/* -------------------------------------------------------------------------
function for determining if a node is within a circle or not
--------------------------------------------------------------------------*/
bool in_circle(double x, double y, double M_x, double M_y, double r)
{
	if (x<M_x-r) // grid point is left of the circle
	{
		return(0);
	}
	else
	{
		if (x>M_x+r) // grid point is right of the circle
		{
			return(0);
		}
		else
		{
			if (y>M_y+pow(r*r-(x-M_x)*(x-M_x),0.5)) // grid point is above the circle
			{
				return(0);
			}
			else
			{
				if (y<M_y-pow(r*r-(x-M_x)*(x-M_x),0.5)) // grid point is below the circle
				{
					return(0);
				}
				else // grid point is within circle
				{
					return(1);
				}
			}
		}
	}
}

double in_circle_diffuse(double x, double y, double M_x, double M_y, double r, double densityl, double densityg, double interface_thickness)
{
	return (0.5*(densityg + densityl) + 0.5*(densityg - densityl)*tanh(2.0/interface_thickness*(sqrt( (x - M_x)*(x - M_x) + (y - M_y)*(y - M_y) ) - r)));
}



/* -------------------------------------------------------------------------
function that returns the maximum of two double variables
--------------------------------------------------------------------------*/
double max(double a, double b)
{
	if (a>b)
	{
		return (a);
	}
	else
	{
		return (b);
	}
}


double sgn(double a)
{
	if (a<0.) {return (-1.);}
	else {return (+1.);}
}


inline int posf(int x, int y)
{
	return (x+lx*y);
}
