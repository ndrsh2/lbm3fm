################################################################################
# Thermal Multiphase Lattice Boltzmann Solver - V1.0
# Copyright (C) 2016 Andreas Hantsch (lbm3f@gmx-topmail.de)
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
################################################################################

comp:
	g++ -Wall -Wextra -ansi -pedantic -O3 -o dev main.cpp

run:
	time ./dev | tee log

post:
	python postprocessing.py

debug:
	g++ -Wall -Wextra -ansi -pedantic -O0 -o dev_debug main.cpp
