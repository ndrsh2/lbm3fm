/*******************************************************************************
* Thermal Multiphase Lattice Boltzmann Solver - V1.0
* Copyright (C) 2016 Andreas Hantsch (lbm3f@gmx-topmail.de)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*******************************************************************************/


#include <iostream>
#include <vector>
#include <cmath>
#include <fstream>
#include <sstream>
#include <string.h>
#include <algorithm>

// Global variables
double intdensity0=0.;
int lx, ly;

using namespace std;
#include "functions.hh"
#include "io.hh"
#include "lbm.hh"


int main()
{
	// intro
	intro();

	// declaration of variables
	int r, D, time=0, time_end, time_check, time_write, time_init;
	double rho_vapour, rho_liquid, nu, tau_liquid, tau_vapour, theta;
	double sigma, beta, kappa, gamma=0.;
	double mufact;
	vector<double> gravity(2);
	vector<double> dp(2);

	// read controllDict
	read_parameter(lx, ly, r, D, time_end, time_check, time_write, time_init, rho_vapour, rho_liquid, nu, sigma, gamma, gravity, dp, tau_liquid, tau_vapour, theta, mufact);

	// distribution functions
	vector<double> g0(9*lx*ly);
	vector<double> g1(9*lx*ly);
	vector<double> h0(9*lx*ly);
	vector<double> h1(9*lx*ly);

	// macroscopic hydrodynamics
	vector<double> rho(lx*ly);
	vector<double> mu(lx*ly);
	vector<double> ux(lx*ly);
	vector<double> uy(lx*ly);
	vector<double> eps(lx*ly);

	// initialization
	init_density(r, D, rho_vapour, rho_liquid, rho, eps, ux, uy, g0, h0, intdensity0);
	calc_parameters(beta, kappa, D, sigma, rho_vapour, rho_liquid);
	calc_chem_potential(lx, ly, rho, eps, beta, kappa, gamma, rho_vapour, rho_liquid, mu, D, time, time_init, theta, mufact);
	write_results(lx, ly, rho, eps, ux, uy, mu, time, time_init, time_end, rho_liquid, rho_vapour);

	// lbm calculation
	cout << "Starting LBM Calculations." << endl << endl;
	for (time=0;time<time_end+1;time++)
	{
		// streaming step
		streaming_step(lx, ly, g0, g1, h0, h1);

		// bounce back at top and bottom wall
		boundary(g1, h1, ux, uy, rho, mu, eps, gravity, time, time_init, beta, rho_liquid, rho_vapour);

		// calculate macroscopic hydrodynamics
		calc_density(lx, ly, rho, g1);
		calc_chem_potential(lx, ly, rho, eps, beta, sigma, gamma, rho_vapour, rho_liquid, mu, D, time, time_init, theta, mufact);
		calc_temperature(lx, ly, eps, h1);

		// check density
		if ((time%time_check)==0){
			check_density(lx, ly, rho, ux, uy, time, intdensity0, time_check);}
			//check_density(lx, ly, time, rho, intdensity0);}

		// collision step
		collision_step(lx, ly, nu, rho, eps, mu, ux, uy, g0, g1, h0, h1, gravity, dp, rho_liquid, rho_vapour, tau_liquid, tau_vapour,D);

		// write results
		if ((time%time_write)==0 && time>0 && time>time_init-1){
			calc_density(lx, ly, rho, g0);
			calc_chem_potential(lx, ly, rho, eps, beta, sigma, gamma, rho_vapour, rho_liquid, mu, D, time, time_init, theta, mufact);
			write_results(lx, ly, rho, eps, ux, uy, mu, time, time_init, time_end, rho_liquid, rho_vapour);
		}

	}
	cout << "LBM Calculations...done." << endl;

	// write results
	write_results(lx, ly, rho, eps, ux, uy, mu, time, time_init, time_end, rho_liquid, rho_vapour);

	return(0);
}
