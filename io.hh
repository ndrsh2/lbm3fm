/*******************************************************************************
* Thermal Multiphase Lattice Boltzmann Solver - V1.0
* Copyright (C) 2016 Andreas Hantsch (lbm3f@gmx-topmail.de)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*******************************************************************************/


int intro()
{
	cout << endl;
	cout << "# Thermal Multiphase Lattice Boltzman Solver V1.0, " << endl;
	cout << "# Copyright (C) 2016 Andreas Hantsch (lbm3f@gmx-topmail.de)" << endl;
	cout << "# This program comes with ABSOLUTELY NO WARRANTY; without " << endl;
	cout << "# even the implied warranty of MERCHANTIBILITY or FITNESS" << endl;
	cout << "# FOR A PARTICULAR PURPOSE. See the GNE General Public" << endl;
	cout << "# License for more details." << endl;
	cout << endl;
	return(0);
}

int read_parameter(int& lx, int& ly, int& r, int& D, int& time_end, int& time_check, int& time_write, int& time_init, double& rho_vapour, double& rho_liquid, double& nu, double& sigma, double& gamma, vector<double>& gravity, vector<double>&dp, double& tau_liquid, double& tau_vapour, double& theta, double& mufact)
{
	cout << "Reading controlDict  . . . . . . . . . ";

	string parameter;
	ifstream controllDict;
	controllDict.open("./system/controlDict", ios::in);

	while (!controllDict.eof()){
		controllDict >> parameter;
		if (parameter == "lx")										{controllDict >> lx;}
		if (parameter == "ly")										{controllDict >> ly;}
		if (parameter == "radius")								{controllDict >> r;}
		if (parameter == "interface_thickness")		{controllDict >> D;}
		if (parameter == "time_end")							{controllDict >> time_end;}
		if (parameter == "time_check")						{controllDict >> time_check;}
		if (parameter == "time_write")						{controllDict >> time_write;}
		if (parameter == "time_init")							{controllDict >> time_init;}
		if (parameter == "density_vapour")				{controllDict >> rho_vapour;}
		if (parameter == "density_liquid")				{controllDict >> rho_liquid;}
		if (parameter == "tau_vapour")						{controllDict >> tau_vapour;}
		if (parameter == "tau_liquid")						{controllDict >> tau_liquid;}
		if (parameter == "viscosity")							{controllDict >> nu;}
		if (parameter == "surface_tension")				{controllDict >> sigma;}
		if (parameter == "gamma")									{controllDict >> gamma;}
		if (parameter == "theta")									{controllDict >> theta;}
		if (parameter == "mufact")								{controllDict >> mufact;}
		if (parameter == "gravity")								{controllDict >> gravity[0] >> gravity[1];}
		if (parameter == "dp")										{controllDict >> dp[0] >> dp[1];}
	}
	controllDict.close();
	theta=theta/180.*pi;
	cout << "done." << endl;
	return(0);
}


string filename(int time, int time_end)
{
	int i=1;
	string space = "";

	for (i=1;i<time_end+1;i=i*10){
		if (time<i){
			space += "0";
		}
	}

	if (time!=time_end && time!=0 && time==10*i){
		space += "0";
	}
	if (time==0){
		space.erase(space.length()-1);
	}

	stringstream timestrings;
	timestrings << space << time;

	string timestring = timestrings.str();
	return (timestring);
}



/*-------------------------------------------------------------------------
function that writes results into files
--------------------------------------------------------------------------*/
int write_results(int lx, int ly, vector<double>& rho, vector<double>& eps, vector<double>& ux, vector<double>& uy, vector<double>& mu, int time, int time_init, int time_end, double rho_liquid, double rho_vapour)
{
	int x,y,pos,y1,y2;
	double temp_loc, yint, rho_avg=0.5*(rho_liquid+rho_vapour);
	FILE *fp;
 	char fileresults[40];

	// fluid domain information
	string name = filename(time, time_end);
	name = "./results/" + name + "_m.ssv";
	strcpy(fileresults,name.c_str());
	fp=fopen(fileresults, "w");

	fprintf(fp,"#x\t\ty\t\trho\t\tux\t\tuy\t\tmu\n");
	for (y=0;y<ly;++y){
		for (x=0;x<lx;++x){
			pos=posf(x,y);
			fprintf(fp,"%.6e\t%.6e\t%.3e\t%.3e\t%.3e\t%.3e\t%.3e\n",double(x),double(y),rho[pos],ux[pos],uy[pos],mu[pos],eps[pos]);
		}
	fprintf(fp, "\n");
	}
	fclose(fp);

	// wall information
	name = filename(time, time_end);
	name = "./results/" + name + "_w.ssv";
	strcpy(fileresults,name.c_str());
	fp=fopen(fileresults, "w");

	fprintf(fp,"x\t\tt_w\n");
	for (x=0;x<lx;++x){
		y=0;
		do{
			y++;
			pos = posf(x,y);
		}
		while(!(rho[pos]<=rho_avg && rho[posf(x,y-1)]>rho_avg) && y<ly);
		yint = y-1. + (rho_avg-rho[posf(x,y-1)])/(rho[pos]-rho[posf(x,y-1)]);
		fprintf(fp,"%.6e\t%.3e\t%.4e\n",double(x),eps[x],yint);
	}
	fclose(fp);

	// mid-channel temperature
	x=int(0.5*lx);
	y1=int(0.5*ly-0.5);
	y2=int(0.5*ly-0.5)-1;
	fp=fopen("./results_temperature.ssv", "a");
	temp_loc= eps[posf(x,y1)]+0.5 *(eps[posf(x,y1)] - eps[posf(x,y2)]); // extrapolation is OK due to the linear analytical solution
	fprintf(fp,"%d\t%.15e\n",time,temp_loc);
	fclose(fp);
	return(0);
}
